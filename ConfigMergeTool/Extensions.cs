﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigMergeTool
{
    static public class Extensions
    {        
        public static string CapLength(this string value, int maxLength)
        {
            if ( value == null )
            {
                return "";
            }

            if (value.Length <= maxLength)
            {
                return value;
            }

            return value.Substring(0, maxLength);
        }
        public static string StripNewLines(this string value)
        {
            return value
                        .Replace("\r\n", " ")
                        .Replace("\n", " ")
                        .Replace("\r", " ");
        }
    }
}
