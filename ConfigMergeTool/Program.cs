﻿using Microsoft.Identity.Client;
using System;
using System.Net;
using System.Security;
using System.Linq;

namespace ConfigMergeTool
{
    class Program
    {
      


        public static void Main(string[] args)
        {
            /*  Entities of Intrest
             *  ---------------------
             *  
             *  [[ Unified Interface ]]
             *  appconfig
             *  appconfiginstance
             *  appconfigmaster
             *  appmodule
             *  appmodulecomponent
             *  appmoduleroles  
             *  columnmapping
             *  complexcontrol
             *  connection          ::  Relationships between entities
             *  connectionrole
             *  customcontrol
             *  customcontroldefaultconfig
             *  dependency          ::  Component dependencies
             *  entitydataprovider
             *  fieldpermission
             *  fieldsecurityrole
             *  lookupmapping
             *  picklistmapping
             *  pluginassembly
             *  plugintype
             *  privilege
             *  role
             *  roleprivileges
             *  roletemplates
             *  roletemplateprivileges
             *  sdkmessage
             *  sdkmessagefilter
             *  sdkmessageprocessingstep
             *  sdkmessageprocessingstepimage
             *  sdkmessageprocessingstepsecureconfig
             *  solution
             *  solutioncomponent
             *  systemform
             *  
             *  https://docs.microsoft.com/en-us/powerapps/developer/common-data-service/webapi/create-update-entity-definitions-using-web-api
             */
            var dev = new Dynamics365Environment();

            var solutions = dev.GetSolutions().Result;

            var unmanagedSolutions = (from s in solutions
                                      where s.IsManaged == false
                                      orderby s.UniqueName
                                      select s).ToArray();



            var components = dev
                                .GetSolutionComponents(unmanagedSolutions
                                                            .First()
                                                            .SolutionId
                                                            .Value)
                                .Result;

            components = components.OrderBy(c => c.ComponentType).ToArray();

            foreach(var c in components)
            {
                Console.WriteLine("{0}  {1}", c.ObjectId, c.ComponentType);
            }

            Console.ReadLine();
        }
    }
}
