﻿using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ConfigMergeTool.Models;

namespace ConfigMergeTool
{
    public class Dynamics365Environment
    {
        private string AccessToken = null;

        public async Task<string> GetAccessToken()
        {
            string clientId = "3c8ecaf5-1970-4fae-b89f-ea313e0b0d19";
            string username = "d365service@conferencetech.com";


            SecureString password = new NetworkCredential("", "KBQT9Xi7sS").SecurePassword;


            IPublicClientApplication app;
            app = PublicClientApplicationBuilder.Create(clientId)
                                                .WithAuthority("https://login.microsoftonline.com/cc702a4a-fdc0-4c82-970f-5699766cee82")
                                                .Build();

            var result = await app.AcquireTokenByUsernamePassword(new string[] { "https://ctidev365.crm.dynamics.com/.default" }, username, password).ExecuteAsync();

            return result.AccessToken;
        }

        public async Task<HttpWebRequest> BuildRequest(string url)
        {
            if ( AccessToken == null )
            {
                AccessToken = await GetAccessToken();
            }

            var req = HttpWebRequest.Create(url);

            req.Headers.Add("Authorization", "Bearer " + AccessToken);

            return (HttpWebRequest)req;
        }

        public async Task<T> GetResponse<T>(HttpWebRequest req)
        {
            var res = req.GetResponse();
            string json = null;


            using (Stream stream = res.GetResponseStream())
            using (StreamReader sr = new StreamReader(stream))
            {
                json = await sr.ReadToEndAsync();
            }

            return JsonConvert.DeserializeObject<T>(json);
        }






        public async Task<SolutionModel[]> GetSolutions()
        {
            var req = await BuildRequest("https://ctidev365.crm.dynamics.com/api/data/v9.1/solutions");

            return (await GetResponse<ResultCollection<SolutionModel>>(req)).Value;
        }

        public async Task<SolutionModel> GetSolution(Guid id)
        {
            var req = await BuildRequest(string.Format("https://ctidev365.crm.dynamics.com/api/data/v9.1/solutions({0})", id));

            return await GetResponse<SolutionModel>(req);
        }


        public async Task<SolutionComponentModel[]> GetSolutionComponents(Guid id)
        {
            var req = await BuildRequest(string.Format("https://ctidev365.crm.dynamics.com/api/data/v9.1/solutions({0})/solution_solutioncomponent", id));

            return (await GetResponse<ResultCollection<SolutionComponentModel>>(req)).Value;
        }
        
        public async Task<EntityDefinitionModel> GetEntityDefinition(Guid id)
        {
            var req = await BuildRequest(string.Format("https://ctidev365.crm.dynamics.com/api/data/v9.0/EntityDefinitions({0})", id));

            return await GetResponse<EntityDefinitionModel>(req);
        }


    }
}
