﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigMergeTool.Models
{
    public class SolutionModel
    {
        public string Description { get; set; }

        public string FriendlyName { get; set; }

        public DateTimeOffset? InstalledOn { get; set; }

        public bool IsManaged { get; set; }

        public DateTimeOffset? ModifiedOn { get; set; }

        public Guid? SolutionId { get; set; }
        
        public string UniqueName { get; set; }

        public DateTimeOffset? UpdatedOn { get; set; }

        public string Version { get; set; }
    }
}
