﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigMergeTool.Models
{
    public class SolutionComponentModel
    {
        public SolutionComponentTypeOptions ComponentType { get; set; }

        public Guid? ObjectId { get; set; }
    }
}
