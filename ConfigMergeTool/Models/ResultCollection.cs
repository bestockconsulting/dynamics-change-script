﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigMergeTool.Models
{
    public class ResultCollection<T>
    {
        public T[] Value { get; set; }
    }
}
