﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigMergeTool.Models
{
    public enum SolutionComponentTypeOptions
    {
        Entity = 1,
        Attribute = 2,
        OptionSet = 9,
        EntityRelationship = 10,
        SavedQuery = 26,
        SystemForm = 60,
        WebResource = 61
    }
}
